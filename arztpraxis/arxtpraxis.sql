-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema arztpraxis
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `arztpraxis` ;

-- -----------------------------------------------------
-- Schema arztpraxis
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `arztpraxis` DEFAULT CHARACTER SET utf8 ;
USE `arztpraxis` ;

-- -----------------------------------------------------
-- Table `arztpraxis`.`person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `arztpraxis`.`person` (
  `per_id` INT NOT NULL AUTO_INCREMENT,
  `per_vname` VARCHAR(45) NOT NULL,
  `per_nname` VARCHAR(45) NOT NULL,
  `per_svnr` VARCHAR(4) NOT NULL,
  `per_geburt` DATE NOT NULL,
  PRIMARY KEY (`per_id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `per_geburt_UNIQUE` ON `arztpraxis`.`person` (`per_geburt` ASC);

CREATE UNIQUE INDEX `per_svnr_UNIQUE` ON `arztpraxis`.`person` (`per_svnr` ASC);


-- -----------------------------------------------------
-- Table `arztpraxis`.`diagnose`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `arztpraxis`.`diagnose` (
  `dia_id` INT NOT NULL AUTO_INCREMENT,
  `dia_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`dia_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `arztpraxis`.`behandlungszeitraum`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `arztpraxis`.`behandlungszeitraum` (
  `ter_id` INT NOT NULL,
  `ter_beginn` DATETIME NOT NULL,
  `ter_ende` DATETIME NOT NULL,
  `dia_id` INT NOT NULL,
  `per_id` INT NOT NULL,
  PRIMARY KEY (`ter_id`),
  CONSTRAINT `fk_behandlungszeitraum_person`
    FOREIGN KEY (`per_id`)
    REFERENCES `arztpraxis`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_behandlungszeitraum_diagnose1`
    FOREIGN KEY (`dia_id`)
    REFERENCES `arztpraxis`.`diagnose` (`dia_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_behandlungszeitraum_person_idx` ON `arztpraxis`.`behandlungszeitraum` (`per_id` ASC);

CREATE INDEX `fk_behandlungszeitraum_diagnose1_idx` ON `arztpraxis`.`behandlungszeitraum` (`dia_id` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `arztpraxis`.`person`
-- -----------------------------------------------------
START TRANSACTION;
USE `arztpraxis`;
INSERT INTO `arztpraxis`.`person` (`per_id`, `per_vname`, `per_nname`, `per_svnr`, `per_geburt`) VALUES (1, 'Roswitha', 'Maier', '1234', '1970-10-05');
INSERT INTO `arztpraxis`.`person` (`per_id`, `per_vname`, `per_nname`, `per_svnr`, `per_geburt`) VALUES (2, 'Hannes', 'Lauter', '8273', '2001-05-22');
INSERT INTO `arztpraxis`.`person` (`per_id`, `per_vname`, `per_nname`, `per_svnr`, `per_geburt`) VALUES (3, 'Karin', 'Ulmer', '9911', '2016-09-10');
INSERT INTO `arztpraxis`.`person` (`per_id`, `per_vname`, `per_nname`, `per_svnr`, `per_geburt`) VALUES (4, 'Walter', 'Welsner', '2423', '1956-07-18');

COMMIT;


-- -----------------------------------------------------
-- Data for table `arztpraxis`.`diagnose`
-- -----------------------------------------------------
START TRANSACTION;
USE `arztpraxis`;
INSERT INTO `arztpraxis`.`diagnose` (`dia_id`, `dia_name`) VALUES (1, 'Grippaler Infekt');
INSERT INTO `arztpraxis`.`diagnose` (`dia_id`, `dia_name`) VALUES (2, 'Influenza');
INSERT INTO `arztpraxis`.`diagnose` (`dia_id`, `dia_name`) VALUES (3, 'Gastritis');

COMMIT;


-- -----------------------------------------------------
-- Data for table `arztpraxis`.`behandlungszeitraum`
-- -----------------------------------------------------
START TRANSACTION;
USE `arztpraxis`;
INSERT INTO `arztpraxis`.`behandlungszeitraum` (`ter_id`, `ter_beginn`, `ter_ende`, `dia_id`, `per_id`) VALUES (1, '2020-01-31 10:15:22', '2020-02-10 15:22:30', 3, 1);
INSERT INTO `arztpraxis`.`behandlungszeitraum` (`ter_id`, `ter_beginn`, `ter_ende`, `dia_id`, `per_id`) VALUES (2, '2023-02-25 14:36:01', '2023-03-01 14:40:10', 1, 1);
INSERT INTO `arztpraxis`.`behandlungszeitraum` (`ter_id`, `ter_beginn`, `ter_ende`, `dia_id`, `per_id`) VALUES (3, '2023-03-10 07:52:22', '2023-04-05 07:54:01', 2, 1);
INSERT INTO `arztpraxis`.`behandlungszeitraum` (`ter_id`, `ter_beginn`, `ter_ende`, `dia_id`, `per_id`) VALUES (4, '2023-03-10 08:01:10', '2023-03-10 08:10:01', 1, 2);
INSERT INTO `arztpraxis`.`behandlungszeitraum` (`ter_id`, `ter_beginn`, `ter_ende`, `dia_id`, `per_id`) VALUES (5, '2023-03-10 08:14:13', '2023-03-14 08:24:22', 1, 3);
INSERT INTO `arztpraxis`.`behandlungszeitraum` (`ter_id`, `ter_beginn`, `ter_ende`, `dia_id`, `per_id`) VALUES (6, '2024-03-15 09:20:10', '', 3, 1);
INSERT INTO `arztpraxis`.`behandlungszeitraum` (`ter_id`, `ter_beginn`, `ter_ende`, `dia_id`, `per_id`) VALUES (7, '2024-04-10 10:15:02', DEFAULT, 3, 1);
INSERT INTO `arztpraxis`.`behandlungszeitraum` (`ter_id`, `ter_beginn`, `ter_ende`, `dia_id`, `per_id`) VALUES (8, '2023-04-20 16:15:30', '', 3, 1);

COMMIT;

