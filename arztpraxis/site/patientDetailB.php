<?php
    //Niklas Grübl 03.04.2024
?>
<form data-sb-form-api-token="API_TOKEN" method="post" class="mt-2 mb-2">
    <h3>Patienten Diagnose</h3>
    <div class="mb-2 row">
        <label class="col-sm-2 form-label" for="svnr">SV-Nummer</label>
        <input class="form-control mediumFormControl" required placeholder="Format: XXXX/JJJJ-MM-TT z.B.: 1234/1999-01-01" 
            pattern="\d{4}/\d{4}-\d{2}-\d{2}" name="svnr" />
    </div>
    <h5>Behandlungsbeginn</h5>
    <div class="form-check">
        <input class="form-check-input" type="radio" value="act" name="begin" id="actMonth" checked>
        <label class="form-check-label" for="actMonth">Aktueller Monat</label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="begin" value="last" id="lastMonth" >
        <label class="form-check-label" for="lastMonth">Vormonat</label>
    </div>
    <div class="input-group">
        <div class="input-group-text">
            <input class="form-check-input mt-0" type="radio" value="custom" name="begin" aria-label="Radio button for following text input">
        </div>
        <input type="text" name="customMonth" placeholder="Monat des laufenden Jahr angeben z.B. 4" class="form-control" aria-label="Text input with radio button">
    </div>
    <input class="btn btn-outline-success mt-2" type="submit" value="Suche starten">
</form>
<?php
if(isset($_POST['svnr'])){
    $params = explode('/',$_POST['svnr']);
    $month;
    switch($_POST['begin']){
        case 'act':
            $month = 'this month';
            break;
        case 'last':
            $month = 'previous month';
            break;
        case 'custom':
            $actYear = date("Y");
            if(!isset($_POST['customMonth']) || $_POST['customMonth'] == ""){
                echo '<h5>Geben Sie einen Monat an</h5>';
                return;
            }
            $actMonth = date("m");
            if($_POST['customMonth'] > $actMonth){
                echo '<h5>Sie haben den Monat '.$_POST['customMonth'].' gewählt. Wir haben aber erst den '.$actMonth.'</h5>';
                return;
            }
            $month = $actYear.'-'.$_POST['customMonth'];
            break;
    }
    $begindate = new DateTime('first day of '.$month);
    $params[] = $begindate->format("Y-m-d").' 00:00:00';
    $enddate = new DateTime('last day of '.$month);
    $params[] = $enddate->format("Y-m-d").' 23:59:59';
    ?>
    <h4 class="mb-3">Suchkriterien:</h4>
    <?php
    echo('<h6>SV-Nr.: '.$_POST['svnr'].'</h6>');
    echo('<h6>Beginnzeitraum: '.$begindate->format("d.m.Y").' - '.$enddate->format("d.m.Y").'</h6>');
    $query = "SELECT concat_ws(' - ', te.ter_beginn, te.ter_ende) as 'Termin', concat_ws(' ', per_vname, per_nname) as 'Patient',
                    concat_ws('/', pe.per_svnr, pe.per_geburt) as 'SVNR', di.dia_name as 'Diagnose' 
                from behandlungszeitraum as te
                inner join person as pe on pe.per_id = te.per_id
                inner join diagnose as di on di.dia_id = te.dia_id
                where pe.per_svnr = ? and pe.per_geburt = ? and te.ter_beginn between ? and ?;";
    makeTable($query, $params);
}