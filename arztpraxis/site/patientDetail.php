<?php
    //Niklas Grübl 03.04.2024
?>
<form data-sb-form-api-token="API_TOKEN" method="post" class="mt-2 mb-2">
    <h3>Patienten Diagnose</h3>
    <div class="mb-2 row">
        <label class="col-sm-2 form-label" for="svnr">SV-Nummer</label>
        <input class="form-control mediumFormControl" required placeholder="Format: XXXX/JJJJ-MM-TT z.B.: 1234/1999-01-01" 
            pattern="\d{4}/\d{4}-\d{2}-\d{2}" name="svnr" id="svnr" />
    </div>
    <h5>Behandlungszeitraum</h5>
    <div class="mb-3 row">
        <label for="dateFrom" class="col-sm-1 col-form-label">von</label>
        <div class="col-sm-2">
            <input id="dateFrom" type="date" class="form-control" name="dateFrom">
        </div>
    </div>
    <div class="mb-3 row">
        <label for="dateTo" class="col-sm-1 col-form-label">bis</label>
        <div class="col-sm-2">
            <input id="dateTo" type="date" class="form-control" name="dateTo">
        </div>
    </div>
    <input class="btn btn-outline-success mt-2" type="submit" value="Suche starten">
</form>
<?php
if(isset($_POST['svnr'])){
    ?>
    <h4 class="mb-3">Suchkriterien:</h4>
    <?php
    $additionalQuery = '';
    echo('<h6>SV-Nr.: '.$_POST['svnr'].'</h6>');
    $params = explode('/',$_POST['svnr']);
    if(isset($_POST['dateFrom']) && $_POST['dateFrom'] != ""){
        echo('<h6>Startzeitraum: '.$_POST['dateFrom'].'</h6>');
        $additionalQuery .= ' and  te.ter_beginn >= ?';
        $params[] = $_POST['dateFrom'];
    }
    if(isset($_POST['dateTo']) && $_POST['dateTo'] != ""){
        echo('<h6>Endzeitraum: '.$_POST['dateTo'].'</h6>');
        $additionalQuery .= ' and  te.ter_ende <= ?';
        $params[] = $_POST['dateTo'];
    }
    $query = "SELECT concat_ws(' - ', te.ter_beginn, te.ter_ende) as 'Termin', concat_ws(' ', per_vname, per_nname) as 'Patient',
                    concat_ws('/', pe.per_svnr, pe.per_geburt) as 'SVNR', di.dia_name as 'Diagnose' 
                from behandlungszeitraum as te
                inner join person as pe on pe.per_id = te.per_id
                inner join diagnose as di on di.dia_id = te.dia_id
                where pe.per_svnr = ? and pe.per_geburt = ?".$additionalQuery.";";
    makeTable($query, $params);
}