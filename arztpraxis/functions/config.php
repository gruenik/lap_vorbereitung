<?php
/*  N.Gruebl, 03.04.24
    Verbindung zur DB und Schema */
try {
    $server = 'localhost:3306'; 
    //Port zur DB Standard 3306
    //kann auch weggelassen werden
    $user = 'root';
    $password = '';
    $schema = 'arztpraxis';

    $connection = new PDO("mysql:host=$server;dbname=$schema;charset=utf8", $user, $password);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
    echo $e->getCode().': '.$e->getMessage();
}