
<!--  Niklas Grübl 14.02.2024
    Alle Bücher welche im Eingegebenen Zeitraum verliehen worden sind anzeigen
-->
<h2>Verliehene Bücher in einem gewissen Zeitraum anezigen:</h2>
<h5>Beachten Sie: Hier wird nur im Tag der Verleihung gesucht! Egal wann die Bücher (wenn überhaupt) zurückgegeben wurden.</h5>
<form data-sb-form-api-token="API_TOKEN" method="post" class="mt-2 mb-2">
    <div class="form-floating ">
        <input required class="form-control" type="date" name="dateFrom" /><br>
        <label for="className">von Datum</label>
    </div>
    <div class="form-floating">
        <input required class="form-control" type="date" name="dateTo" /><br>
        <label for="className">bis Datum</label>
    </div>
    <input class="btn btn-outline-success" type="submit" value="Suche starten">
</form>

<?php

if(isset($_POST['dateFrom']) && isset($_POST['dateTo'])){

    if($_POST['dateFrom'] > $_POST['dateTo']){
        echo '<h6 class="mt-5">Enddatum muss nach Beginndatum liegen!</h6>';
        return;
    }
    
    // Abfrage lt. Test
    // $query = "SELECT ve.ver_start as Startdatum, ve.ver_retour as Retourdatum, buc_name as Buchtitel, ve.exe_id as ExemplarNr, concat(pe.per_vname,' ' ,pe.per_nname) as Person
    // from verleih as ve
    // inner join exemplar as ex on ex.exe_id = ve.exe_id
    // inner join buchtitel as bu on bu.buc_id = ex.buc_id
    // inner join person as pe on pe.per_id = ve.per_id
    // where ve.ver_start between ? and ?
    // order by ve.ver_start;";

    $query = "SELECT buc_name as Buchtitel, ve.exe_id as ExemplarNr, ve.ver_start as Startdatum, ve.ver_retour as Retourdatum, concat(pe.per_vname,' ' ,pe.per_nname) as Person
    from verleih as ve
    inner join exemplar as ex on ex.exe_id = ve.exe_id
    inner join buchtitel as bu on bu.buc_id = ex.buc_id
    inner join person as pe on pe.per_id = ve.per_id
    where ve.ver_start between ? and ?
    order by ve.ver_start;";
    $array = array($_POST['dateFrom'], $_POST['dateTo']);
    makeTable($query, $array);
}else{
    echo '<h6 class="mt-5">Geben Sie die Datumsgrenzen ein</h6>';
}
