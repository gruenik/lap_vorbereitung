
<!--  Niklas Grübl 14.02.2024
    Alle Schulklassen in Tabelle ausgeben
-->
<h1>Schulklassen</h1>

<form data-sb-form-api-token="API_TOKEN" method="post" class="mt-2 mb-1">
    <div class="input-group mb-3">
        <input type="text" class="form-control" placeholder="Suche" name="searchValue">
        <div class="input-group-append">
            <input type="submit" class="btn btn-outline-success" value="Suchen" />
        </div>
    </div>
</form>

<?php
$query = "select sccl_id as 'ID', sccl_name as 'Klasse' from school_class where sccl_name like ? order by sccl_name";
$searchValue = '';
if(isset($_POST["searchValue"])){
    $searchValue = $_POST["searchValue"];
}
$searchValue = '%'.$searchValue.'%';
$array = array($searchValue);
makeTable($query, $array);
