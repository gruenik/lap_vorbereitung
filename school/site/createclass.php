<!--Niklas Grübl 14.02.2024
    Neue Klasse anlegen-->
<h1>Klasse anlegen</h1>
<form data-sb-form-api-token="API_TOKEN" method="post" class="mb-5">
    <div class="form-floating mb-2">
        <input class="form-control" type="text" name="className" /><br>
        <label for="className">Klassenname</label>
    </div>
    <div class="d-grid">
        <input class="btn btn-primary btn-lg" name="save" type="submit" />
    </div>
</form>
<?php
    if(isset($_POST["save"]) && $_POST["className"] !== "") {
        $className = $_POST["className"];
        createClass($className);
    }
