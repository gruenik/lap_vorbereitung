<?php 
/*  Niklas Grübl, 13.02.2024
    Funktionen/Prozeduren */
function getSite($site){
    if(isset($_GET['site'])){
        include_once('site/'. $_GET['site'] .'.php');
    }else{
        include_once('site/'.$site.'.php');
    } 
}
function makeStatement($query, $array = null){
    try {
        global $connection;
        $statement = $connection->prepare($query);
        $statement->execute($array);
        return $statement;
    } catch (Exception $e) {
        return $e;
    }
}
function makeTable($query, $array = null){
    $stmt = makeStatement($query, $array);
    if($stmt instanceof Exception){
        echo $stmt->getCode().': '.$stmt->getMessage();
    }else{
        if($stmt->rowCount() == 0){
            echo '<h2>Keine Daten gefunden</h2>';
        }else{
            //Tabelle erstellen
            $meta = array();
            echo '<table class="table"><tr>';
            for($i = 0; $i <$stmt->columnCount(); $i++){
                $meta[] = $stmt->getcolumnmeta($i);
                echo '<th>'.$meta[$i]['name'].'</th>';
            }
            echo '</$stmt->';
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                echo '<tr>';
                foreach($row as $r){
                    echo '<td>'.$r.'</td>';
                }
                echo '</tr>';
            }
            echo '</table>';
        }
    }
}
function createClass($className){
    try {
        $query = 'insert into school_class(sccl_name) values(?)';
        $array = array($className);
        $stmt = makeStatement($query, $array);
        if( $stmt instanceof Exception){
            echo '<h3>Klasse '.$className.' kann nicht gespeichert werden!</h3>';
        }else{
            echo '<h3>Klasse '.$className.' wurde gespeichert</h3>';
        }
    } catch (Exception $e) {
        $e->getCode().': '.$e->getMessage();
    }
}