<?php
require '../functions/connection.php';

$sql = "SELECT productCode, productName, productLine, productScale, productVendor, quantityInStock, buyPrice FROM products;";
$stmt = $connection->prepare($sql);
$stmt->execute();
$productline = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($productline);