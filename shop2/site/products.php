<h1>Produkte</h1>
<table class="table" id="productTable">
    <tr>
        <th>ID</th>
        <th>Bezeichnung</th>
        <th>Produktgruppe</th>
        <th>Maßstab</th>
        <th>Disponent</th>
        <th>Verfügbare Einheiten</th>
        <th>Preis</th>
        <th>Menge</th>
        <th></th>
    </tr>
    <button class="btn btn-outline-primary" type="button" onclick="getProductRows()">Inhalt laden</button>
    <?php
    makeProductTable();
    if (isset($_POST["addProduct"])) {
        if (!isset($_SESSION['basket'])) {
            $_SESSION['basket'] = array();
        }
        $newProduct = new stdClass();
        $newProduct->productID = $_POST['id'];
        $newProduct->quantity = $_POST['menge'];
        $newProduct->priceEach = $_POST['price'];
        $_SESSION['basket'] = array_merge($_SESSION['basket'], array($newProduct));
    }
    ?>
    <script>
        function getProductRows() {
            const productApi = "./api/getProducts.php";
            fetch(productApi)
            .then(response => response.json())
            .then(produktlines => {
            const productTable = document.getElementById("productTable").tBodies[0];
            productTable.innerHTML = ''; // Liste leeren bevor neue Einträge hinzugefügt werden
            const tableColumns = new Map([
                ["productCode", "ID"],
                ["productName", "Bezeichnung"],
                ["productLine", "Produktlinie"],
                ["productScale", "Maßstab"],
                ["productVendor", "Disponent"],
                ["quantityInStock", "Verfügbare Einheiten"],
                ["buyPrice", "Kaufpreis"]
            ]);
            const headerLine =document.createElement('tr');
            tableColumns.forEach(column => {
                const headerColumn = document.createElement('th');
                headerColumn.textContent = column;
                headerLine.appendChild(headerColumn);
            });
            productTable.appendChild(headerLine);
            produktlines.forEach(produktline => {
                const productRow = document.createElement('tr');
                tableColumns.keys().forEach(column => {
                    const newColumn = document.createElement('td');
                    newColumn.textContent = produktline[column];
                    productRow.appendChild(newColumn)
                });
                // produktlinesListElement.textContent = `${produktline.productLine}`;
                productTable.appendChild(productRow);
            });
            })
            .catch(error => console.error('Error:', error));
        }
    </script>