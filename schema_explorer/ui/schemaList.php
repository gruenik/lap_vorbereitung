<?php 
    $query = 'show schemas;';
    $stmt = makeStatement($query);
?>
<div class="list-group list-group-flush border-bottom scrollarea">
</$stmt->
<?php
    $schemas = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($schemas as $schema) {
        ?>
        <a href="?schema=<?php echo $schema['Database'] ?>" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 align-items-center justify-content-between">
                <strong><?php echo $schema['Database'] ?></strong>
            </div>    
        </a>
        <?php
    }
?>
</div>