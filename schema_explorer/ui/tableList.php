<?php
    if(isset($_GET['schema'])){
        $_SESSION['schema'] = $_GET['schema'];
        unset($_SESSION['table']);
        unset($_SESSION['mode']);
    }

    if(isset($_SESSION['schema'])){
        $query = 'select table_name from information_schema.tables where table_schema = ?';
        $stmt = makeStatement($query, array($_SESSION['schema']));
        if($stmt instanceof Exception){
            echo $stmt->getCode().': '.$stmt->getMessage();
            return;
        }
        $tables = $stmt->fetchAll(PDO::FETCH_ASSOC);
        ?>
        <div class="list-group list-group-flush border-bottom scrollarea">
        </$stmt->
        <?php
        foreach($tables as $table){
            ?>
            <!-- <a href="?table=<?php echo $table['table_name'] ?>" class="list-group-item list-group-item-action"> -->
            <div class="list-group-item">
                <div class="d-flex w-100 align-items-center justify-content-between">
                    <strong class="col-md-6"><?php echo $table['table_name'] ?></strong>
                    <a href="?table=<?php echo $table['table_name'] ?>&mode=structure" class="col-md-3 btn btn-outline-primary">Struktur</a>
                    <a href="?table=<?php echo $table['table_name'] ?>&mode=content" class="col-md-3 btn btn-outline-primary">Inhalt</a>
                </div>    
            </div>    
            <!-- </a> -->
            <?php
        }
        ?>
        </div>
        <?php
    }