<?php
    if(isset($_GET['table'])){
        $_SESSION['table'] = $_GET['table']; 
    }
    if(isset($_GET['mode'])){
        $_SESSION['mode'] = $_GET['mode']; 
    }

    if(isset($_SESSION['table'])){
        $query = "";
        $params = null;
        if($_SESSION['mode'] == "content"){ //Inhalt der Tabelle anzeigens
            $query = 'select * from '.$_SESSION['schema'].'.'.$_SESSION['table'].';';
        }elseif($_SESSION['mode'] == "structure"){ //Struktur der Tabelle anzeigen
            $query = 'select column_name, column_type, is_nullable, column_key, extra from information_schema.columns where table_schema = ? and table_name= ?';
            $params = array($_SESSION['schema'], $_SESSION['table']);
        }
        makeTable($query, $params);
    }