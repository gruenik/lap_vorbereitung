<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <?php
  include __DIR__ . '/_menue.php';
  echo "</br>";

  //API = (Application Programming Interface) 
//Eine Programmierschnittstelle - dient der zur Verfügungstellen von DAten-ein- und ausgängen.
//Bsp: Ajax, REST, Crud usw.
//SOAP-Webservices: = Protokoll, etwas älter, XML
  ?>

  <h1>ProduktLinien anzeigen</h1>
  <br><br>
  <div class="line-list">
    <h2>Ausgabe:</h2>
    <ul id="lineList"></ul>
    <!-- request here-->
  </div>
  <button type="button" onclick="laden('_api_getProduktlines.php','lineList')">Inhalt laden</button>

  </br>
  </br>
  <form id="post-form">
    <lable>Name für neue Produktlinie: </lable>
    <input name="linename" />
    <br />
    <Button type="submit">Einfügen</Button>
  </form>
  <div id="response-message"></div>





  <script>





    function laden(url, element) {
      fetch(url)
        .then(response => response.json())
        .then(produktlines => {
          const produktlinesList = document.getElementById(element);
          produktlinesList.innerHTML = ''; // Liste leeren bevor neue Einträge hinzugefügt werden

          produktlines.forEach(produktline => {
            const produktlinesListElement = document.createElement('li');
            produktlinesListElement.textContent = `${produktline.productLine}`;
            produktlinesList.appendChild(produktlinesListElement);
          });
        })
        .catch(error => console.error('Error:', error));
    }





    const apiUrl = '_api_postProduktline.php';

    const contactForm = document.getElementById('post-form');
    const responseMessage = document.getElementById('response-message');

    contactForm.addEventListener('submit', function (event) {
      event.preventDefault();

      const formData = new FormData(contactForm);

      const requestOptions = {
        method: 'POST',
        body: formData,
      };

      fetch(apiUrl, requestOptions)
        .then(response => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.text();
        })
        .then(data => {
          responseMessage.textContent = data;
        })
        .catch(error => {
          console.error('Error:', error);
        });
    });

  </script>

</body>

</html>