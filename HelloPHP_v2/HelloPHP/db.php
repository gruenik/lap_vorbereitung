<!DOCTYPE html>
<html>
<body>

<?php  include __DIR__.'/_menue.php' ?>
<br>
<?php

//zugriff auf mariaDB
//include __DIR__.'/_db_connection.php'; 

// PHP Fehlermeldungen anzeigen
error_reporting(E_ALL);
ini_set('display_errors', true);

// Zugangsdaten zur Datenbank
$DB_HOST = "localhost"; // Host-Adresse
$DB_NAME = "classicmodels"; // Datenbankname
$DB_BENUTZER = "root"; // Benutzername
$DB_PASSWORT = ""; // Passwort

// Zeichenkodierung UTF-8 (utf8mb4) bei der Verbindung setzen,
//Und eine PDOException bei einem Fehler auslösen. 
$OPTION = [
  PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4",
  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
];

try {
  // Verbindung zur Datenbank aufbauen
  $db = new PDO(
    "mysql:host=" . $DB_HOST . ";dbname=" . $DB_NAME,
    $DB_BENUTZER,
    $DB_PASSWORT,
    $OPTION
  );
} catch (PDOException $e) {
  // Bei einer fehlerhaften Verbindung eine Nachricht ausgeben
  exit("Verbindung fehlgeschlagen! " . $e->getMessage());
}


//"SELECT VERSION()"

$sql = "SELECT VERSION() AS 'Version'";
$stmt = $db->query($sql);
$record = $stmt->fetchAll()[0];
echo 'MariaDB Version: '  . $record['Version'] . '</br>';








echo '</br>';

echo "Inhalt productlines: ". '</br>';
//select productLine as line from productlines;
$sql = "select productLine as line from productlines;" ;
$stmt = $db->query($sql);
foreach($stmt->fetchAll() as $pline)
echo  $pline['line']. '</br>';

?>
</br>
<form action= "" method  ="post">
    <lable>Name für neue Produktlinie: </lable>
    <input name ="linename" /> 
    <br/>
    <Button type="submit">Einfügen</Button>
</form>

<?php 


echo '</br>';







// Datensatz einfügen.
//Achtung auf SQL Injections
// 
 if (isset($_POST['linename'])){
    $line =$_POST['linename'];
    $stmt  = $db->prepare(
        "INSERT INTO `productlines` 
        (`productLine`,`textDescription`) 
        VALUES ( ? ,'test info')"
    );
    $stmt ->execute([$line]);
    
    /*
        $db->exec("INSERT INTO `productlines`
        (`textDescription`,`productLine`)
        VALUES ('test info','".$line."')");
    */

/*
INSERT INTO `classicmodels`.`productlines`
(`productLine`,
`textDescription`)
VALUES(
"test",
"test info"
);*/
 
  echo "Danke, Produktlinie ". $_POST['linename'] ." wurde eingefügt.";    
}




?>


</body>
</html>
   