<div class="bg-body-tertiary p-5 rounded">
    <h1>Orte</h1>
    <?php 
        $query = 'select p.plz_nr as PLZ, o.ort_name as Ort
        from ort_plz as op
        inner join plz as p on p.plz_id = op.plz_id
        inner join ort as o on o.ort_id = op.ort_id;';

        makeTable($query);
    ?>
</div>