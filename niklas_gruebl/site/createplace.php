<div class="bg-body-tertiary p-5 rounded">
    <h1>Neuen Ort/Neue Stadt hinzufügen</h1>
    <form data-sb-form-api-token="API_TOKEN" method="post" class="mb-5">
        <div class="form">
            <label for="placeName">Ort-/Stadtname</label>
            <input class="form-control" type="text" name="placeName" /><br>
        </div>
        <div class="form mb-2">
            <label for="placePlz">Postleitzahl</label>
            <select class="form-control" name="placePlz" value="">
                <option disabled selected value="">Wählen Sie eine PLZ</option>
                <?php 
                    getPlzOptions();
                ?>
            </select>
            <!-- <input class="form-control" type="text" name="placePlz" /><br> -->
        </div>
        <div class="d-grid">
            <input class="btn btn-success btn-lg" name="save" type="submit" />
        </div>
    </form>
</div>
<?php
    if(isset($_POST["save"]) && $_POST["placeName"] !== "" && $_POST["placePlz"] !== "") {
        createPlace($_POST["placeName"], $_POST["placePlz"]);
    }