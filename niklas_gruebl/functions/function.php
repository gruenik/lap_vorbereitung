<?php
/*  Niklas Grübl, 13.02.2024
    Funktionen/Prozeduren */
function getSite($site){
    if(isset($_GET['site'])){
        include_once('site/'. $_GET['site'] .'.php');
    }else{
        include_once('site/'.$site.'.php');
    } 
}
function makeStatement($query, $array = null){
    try {
        global $connection;
        $statement = $connection->prepare($query);
        $statement->execute($array);
        return $statement;
    } catch (Exception $e) {
        return $e;
    }
}
function makeTable($query, $array = null){
    $stmt = makeStatement($query, $array);
    if($stmt instanceof Exception){
        echo $stmt->getCode().': '.$stmt->getMessage();
    }else{
        if($stmt->rowCount() == 0){
            echo '<h2>Keine Daten gefunden</h2>';
        }else{
            //Tabelle erstellen
            $meta = array();
            echo '<table class="table"><tr>';
            for($i = 0; $i <$stmt->columnCount(); $i++){
                $meta[] = $stmt->getcolumnmeta($i);
                echo '<th>'.$meta[$i]['name'].'</th>';
            }
            echo '</$stmt->';
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                echo '<tr>';
                foreach($row as $r){
                    echo '<td>'.$r.'</td>';
                }
                echo '</tr>';
            }
            echo '</table>';
        }
    }
}

function getPlzOptions(){
    $query = 'SELECT plz_id, plz_nr FROM plz';
    $stmt = makeStatement($query);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($result as $row){
        echo '<option value = '.$row['plz_id'].'>'.$row['plz_nr'].'</option>';
    }
}

function createPlace($placeName, $plz){
    $query = 'SELECT ort_id from ort where ort_name = ?;';
    $stmt = makeStatement($query, array($placeName));
    if($stmt instanceof Exception){
        echo $stmt->getCode().': '.$stmt->getMessage();
        return;
    }
    if($stmt->rowCount() > 0){
        echo '"'.$placeName.'" ist bereits vorhanden. Hier können nur neue Ortsnamen erfasst werden';
        return;
    }
    $query = 'INSERT INTO ort (ort_name) VALUES(?);';
    $stmt = makeStatement($query, array($placeName));
    if($stmt instanceof Exception){
        echo $stmt->getCode().': '.$stmt->getMessage();
        return;
    }
    global $connection;
    $insertedPlace = $connection->lastInsertId(); //retrieve the id of the created place
    
    $query = 'INSERT INTO ort_plz(ort_id, plz_id) VALUES(?,?);';

    $stmt = makeStatement($query, array($insertedPlace, $plz));
    if($stmt instanceof Exception){
        echo $stmt->getCode().': '.$stmt->getMessage();
        return;
    }
    echo '<script>location.href = location.href.replace("site=createplace","site=places")</script>';
}
