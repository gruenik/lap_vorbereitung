<?php 
/*  Niklas Grübl, 28.02.2024
    Funktionen/Prozeduren */

function makeStatement($query, $array = null){
    try {
        global $connection;
        $statement = $connection->prepare($query);
        $statement->execute($array);
        return $statement;
    } catch (Exception $e) {
        return $e;
    }
}

function getSearchFilters(){
    ?>
    <div class="p-2">
        <form method="post" >
            <div class="form-check">
                <input class="form-check-input" type="radio" name="searchMode" value="original" id="rb_original">
                <label class="form-check-label" for="rb_original">Originalfilm</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="searchMode" value="foreign" id="rb_foreign">
                <label class="form-check-label" for="rb_foreign">Fremdsprachentitel</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="searchMode" value="episode" id="rb_episode">
                <label class="form-check-label" for="rb_episode">Episoden</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="searchMode" value="all" id="rb_all">
                <label class="form-check-label" for="rb_all">alle oben genannten</label>
            </div>
            <div class="d-flex" mt-1>
                <input class="form-control me-2" name="searchValue" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" name="submit" type="submit">Search</button>
            </div>
      </form>
    </div>
    <?php
}

function createFilmList(){

    $searchValue = $_SESSION['searchValue'];
    $searchValue = '%'.$searchValue.'%';
    $whereClause = '';
    $params = array();
    switch ($_SESSION['searchMode']) {
        case 'original':
            $whereClause = ' where (frt.title LIKE ? and frt.original = 1) or (ft.title LIKE ? and ft.original = 1);';
            $params = array($searchValue, $searchValue);
            break;
        case 'foreign':
            $whereClause = ' where (frt.title LIKE ? and isnull(frt.original)) or (ft.title LIKE ? and isnull(ft.original));';
            $params = array($searchValue, $searchValue);
            break;
        case 'episode':
            $whereClause = ' where ft.title LIKE ?';
            $params = array($searchValue);
            break;
        case 'all':
            $whereClause = ' where frt.title LIKE ? or ft.title LIKE ?';
            $params = array($searchValue, $searchValue);
            break;
    }

    $query = "select distinct fi.filmid as filmid, concat( COALESCE( concat(frt.title, ': '), ''), ft.title ) as Filmtitel
        from film as fi
        inner join film_title as ft on ft.filmid = fi.filmid
        left outer join franchise_title as frt on frt.franchiseid = ft.franchise_franchiseid and frt.langid = ft.language"
        .$whereClause;
    // echo $query;
    $stmt = makeStatement($query, $params);
    if($stmt instanceof Exception){
        echo $stmt->getCode().': '.$stmt->getMessage();
    }else{
        if($stmt->rowCount() == 0){
            echo '<h2>Keine Filme verfügbar</h2>';
        }else{
            //Liste erstellen
            ?>
            <div class="list-group list-group-flush border-bottom scrollarea">
            </$stmt->
            <?php
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                ?>
                <a href="?filmId=<?php echo $row['filmid'] ?>" class="list-group-item list-group-item-action py-3 lh-sm">
                    <div class="d-flex w-100 align-items-center justify-content-between">
                        <strong class="mb-1"><?php echo $row['Filmtitel'] ?></strong>
                    </div>
                </a>
                <?php
            }
            ?>
            </div>
            <?php
        }
    }
}

function getFilmDetail(){
    if(!isset($_GET['filmId'])){
        return;
    }
    $filmid = $_GET['filmId'];
    $query = "select concat( COALESCE( concat(frt.title, ': '), ''), ft.title ) as Filmtitel, fi.shortdescription as beschreibung, 
                    fi.releaseyear as releaseyear, fi.posterPath as posterPath
                from film as fi
                inner join film_title as ft on ft.filmid = fi.filmid
                left outer join franchise_title as frt on frt.franchiseid = ft.franchise_franchiseid and frt.langid = ft.language
                where ft.language = 1 and fi.filmid = ?;";
    $stmt = makeStatement($query, array($filmid));
    if($stmt instanceof Exception){
        echo $stmt->getCode().': '.$stmt->getMessage();
        return;
    }
    if($stmt->rowCount() == 0){
        echo '<h2>Film existiert nicht</h2>';
        return;
    }
    $film = $stmt->fetch(PDO::FETCH_ASSOC);
    $posterPath = $film['posterPath'];
    if(!file_exists($_SERVER['DOCUMENT_ROOT'].$posterPath) || $posterPath == ""){
        $posterPath = '/img/image_not_found.jpg';
    }
    ?>
    <div class="parent overflow-auto">
        <div class="child">
            <img width="350px" src="<?php echo $posterPath ?>"></img>
        </div>
        <div class="child">
        <?php
            echo '<h2>'.$film['Filmtitel'].' ('.$film['releaseyear'].')</h2>';
            echo '<h5>'.$film['beschreibung'].'</h5>';
        ?>
        </div>
        <br>
        <div class="child">
            <?php getMainActors($filmid); ?>
        </div>
        <div class="child">
            <?php getDirectors($filmid); ?>
        </div>
    </div>
    <?php
}
function getMainActors($filmid){
    $query = "select firstname, lastname
                from person as pe
                inner join film_actors as fa on fa.personid = pe.personid
                where fa.filmid = ? and fa.mainActor = 1";
    $stmt = makeStatement($query, array($filmid));
    if($stmt instanceof Exception){
        echo $stmt->getCode().': '.$stmt->getMessage();
        return;
    }
    if($stmt->rowCount() == 0){
        echo '<h6>Keine Hauptdarsteller zu Film gefunden</h6>';
        return;
    }
    ?>
    <h6>Hauptdarsteller</h6>
    <ul>
    <?php
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            ?>
            <li>
                <span><?php echo $row['firstname'].' '.$row['lastname']; ?></span>
            </li>
            <?php
        }
    ?>
    </ul>
    <?php
}

function getDirectors($filmid){
    $query = "select firstname, lastname
                from person as pe
                inner join film_directors as fd on fd.personid = pe.personid
                where fd.filmid = ?";
    $stmt = makeStatement($query, array($filmid));
    if($stmt instanceof Exception){
        echo $stmt->getCode().': '.$stmt->getMessage();
        return;
    }
    if($stmt->rowCount() == 0){
        echo '<h6>Keine Regie zu Film gefunden</h6>';
        return;
    }
    ?>
    <h6>Regie</h6>
    <ul>
    <?php
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            ?>
            <li>
                <span><?php echo $row['firstname'].' '.$row['lastname']; ?></span>
            </li>
            <?php
        }
    ?>
    </ul>
    <?php
}