<?php
echo "<h1>Produkte</h1>";

makeProductTable();
if(isset($_POST["addProduct"])){
    if(!isset($_SESSION['basket'])){
        $_SESSION['basket'] = array();
    }
    $newProduct = new stdClass();
    $newProduct->productID = $_POST['id'];
    $newProduct->quantity = $_POST['menge'];
    $newProduct->priceEach = $_POST['price'];
    $_SESSION['basket'] = array_merge($_SESSION['basket'], array($newProduct));
}