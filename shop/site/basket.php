<h1 class="mb-3">Warenkorb</h1>
<?php
if(isset($_POST['orderWarenkorb'])){
    orderWarenkorb();
}
if(!isset($_SESSION['basket']) || count($_SESSION['basket']) == 0){
    ?>
    <h3>Keine Produkte im Warenkorb</h3>
    <?php
}else{
    displayWarenkorb($_SESSION['basket']);
}


function displayWarenkorb($products){
    $allProducts = getWarenkorbProducts($products);
    ?>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Bezeichnung</th>
            <th>Produktgruppe</th>
            <th>Maßstab</th>
            <th>Verfügbare Einheiten</th>
            <th>Preis/Stück</th>
            <th>Stück</th>
            <th>Gesamtpreis</th>
        </tr>
        <?php 
            foreach ($allProducts as $product) {
                getWarenkorbLine($product);
            }
        ?>
    </table>
    <form method="post">
        <input name="orderWarenkorb" class="btn btn-outline-primary" type="submit" value="Bestellen">
    </form>
    <?php
}

function getWarenkorbProducts($products){
    $query = 'SELECT productCode, productName, productLine, productScale,
    quantityInStock, buyPrice FROM products WHERE productCode IN (';
    $whereClause = '';
    $params = array();
    foreach ($products as $key=>$product) {
        $whereClause = $whereClause.'?';
        if($key < count($products) -1){
            $whereClause = $whereClause.',';
        }
        $params = array_merge($params, array($product->productID));
    }
    $query = $query.$whereClause.');';
    $stmt = makeStatement($query, $params);
    $productsDB = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $result = array();
    foreach($productsDB as $item ){
        $newItem = new stdClass();
        $newItem->id = $item["productCode"];
        $newItem->description = $item["productName"];
        $newItem->productline = $item["productLine"];
        $newItem->scale = $item["productScale"];
        $newItem->available = $item["quantityInStock"];
        $newItem->pricePerUnit = $item["buyPrice"];
        $newItem->quantity = getQuantityFromArray($products, $item['productCode']);
        $newItem->priceAll = $newItem->pricePerUnit * $newItem->quantity;
        $result = array_merge($result, array($newItem));
    }
    return $result;
}
function getQuantityFromArray($products, $id){
    foreach($products as $product){
        if($product->productID == $id){
            return $product->quantity;
        }
    }
}
function getWarenkorbLine($product){
    ?>
    <tr>
        <td><?php echo $product->id ?></td>
        <td><?php echo $product->description ?></td>
        <td><?php echo $product->productline ?></td>
        <td><?php echo $product->scale ?></td>
        <td><?php echo $product->available ?></td>
        <td><?php echo $product->pricePerUnit ?></td>
        <td><?php echo $product->quantity ?></td>
        <td><?php echo $product->priceAll ?></td>
    </tr>
    <?php
}

function orderWarenkorb(){
    $queryOrder = "insert into orders (orderDate, requiredDate, status, customerNumber) 
                    values ('2024-03-01','2024-03-15','Ordered',475);";
    $stmt = makeStatement($queryOrder);
    global $connection;
    $insertedOrder = $connection->lastInsertId();

    $queryDetail = "insert into orderdetails (orderNumber, productCode, quantityOrdered, priceEach, orderLineNumber) values";
    $products = $_SESSION['basket']; 
    foreach($products as $key=>$product){
        $queryDetail .= '('.$insertedOrder.',"'.$product->productID.'",'.$product->quantity.','.$product->priceEach.','.($key + 1).')';
        if($key < count($products) - 1 ){
            $queryDetail .= ',';
        }
    }
    makeStatement($queryDetail);

    session_unset();
    ?>
    <p class="text-success">Bestellung mit der Nummer: <?php echo $insertedOrder ?> wurde erstellt</p>
    <?php
    // echo '<p class=text-success">Bestellung mit der Numer: '.$insertedOrder.' wurde erstellt!</p>';
}