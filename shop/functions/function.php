<?php
function getSite($site){
    if(isset($_GET['site'])){
        include_once('site/'. $_GET['site'] .'.php');
    }else{
        include_once('site/'.$site.'.php');
    } 
}
function makeStatement($query, $array = null){
    try {
        global $connection;
        $statement = $connection->prepare($query);
        $statement->execute($array);
        return $statement;
    } catch (Exception $e) {
        echo $e->getCode().': '.$e->getMessage();
    }
}
function makeProductTable($array = null){
    $query = "SELECT productCode as 'ID', productName as 'Bezeichnung', productLine as 'Produktgruppe', productScale as 'Maßstab', productVendor as 'Disponent', 
        quantityInStock as 'Verfügbare Einheiten', buyPrice as 'Preis' FROM products;";
    $stmt = makeStatement($query, $array);
    if($stmt instanceof Exception){
        echo $stmt->getCode().': '.$stmt->getMessage();
    }else{
        //Tabelle erstellen
        $meta = array();
        echo '<table class="table"><tr>';
        for($i = 0; $i <$stmt->columnCount(); $i++){
            $meta[] = $stmt->getcolumnmeta($i);
            echo '<th>'.$meta[$i]['name'].'</th>';
        }
        echo '<th>Menge</th><th></th>';
        echo '</$stmt->';
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($rows as $row){
            echo createProductRow($row);
        }
        echo '</table>';
    }
}
function createProductRow($row){
    $rowtemplate = '<tr><form method="post">';
    $rowtemplate .= '<td><input class="form-control-plaintext" readonly name="id" value="'.$row['ID'].'"></input></td>';
    $rowtemplate .= '<td>'.$row['Bezeichnung'].'</td>';
    $rowtemplate .= '<td>'.$row['Produktgruppe'].'</td>';
    $rowtemplate .= '<td>'.$row['Maßstab'].'</td>';
    $rowtemplate .= '<td>'.$row['Disponent'].'</td>';
    $rowtemplate .= '<td>'.$row['Verfügbare Einheiten'].'</td>';
    $rowtemplate .= '<td><input class="form-control-plaintext" readonly name="price" value="'.$row['Preis'].'"></input></td>';
    $rowtemplate .= '<td><input type="number" name="menge"></input></td>';
    $rowtemplate .= '<td><button name="addProduct" class="btn btn-primary" type="submit"><span class="bi bi-bag-plus-fill"></button></td>';
    return $rowtemplate.'</form></tr>';
}